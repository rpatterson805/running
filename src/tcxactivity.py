import xml.etree.ElementTree as ET
import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as md
from matplotlib.collections import LineCollection
from matplotlib.patches import Circle

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

ns = {'ns1':'http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2', 
    'ns2':'http://www.garmin.com/xmlschemas/UserProfile/v2', 
    'ns3':'http://www.garmin.com/xmlschemas/ActivityExtension/v2', 
    'ns4':'http://www.garmin.com/xmlschemas/ProfileExtension/v1', 
    'ns5':'http://www.garmin.com/xmlschemas/ActivityGoals/v1'}

class tcxactivity:

    def __init__(self, filename):
        """
        read in tcx file data, save activity summary data
        """
        self.filename = filename

        # hold only summary data in memory
        self.summary = self.GetSummaryData()
        
        return None

    #-------------------------------------------------------------------------
    def GetPtData(self):
        """
        parse through tcx file, output dataframe containing individual track
        point data
        """
        tree = ET.parse(self.filename)
        root = tree.getroot()
        # start time
        tstart = datetime.datetime.strptime( 
            root.findall('.//ns1:Id', ns)[0].text, '%Y-%m-%dT%H:%M:%S.%fZ' )
        # trackpoints
        points = root.findall('.//ns1:Lap//ns1:Track//ns1:Trackpoint', ns)

        Distance = np.zeros(len(points))
        RunTime = np.zeros(len(points))
        Speed = np.zeros(len(points))
        HeartRate = np.zeros(len(points))
        Cadence = np.zeros(len(points))
        Latitude = np.zeros(len(points))
        Longitude = np.zeros(len(points))
        Altitude = np.zeros(len(points))

        ct = 0
        oldt = 0.0
        RunTime[-1] = 1.0
        for pt in points:
            # distance
            d = pt.findall('.//ns1:DistanceMeters', ns)
            Distance[ct] = float(d[0].text) if len(d)>0 else np.nan
            # speed
            spd = pt.findall('.//ns1:Extensions//ns3:TPX//ns3:Speed', ns)
            Speed[ct] = float(spd[0].text) if len(spd)>0 else np.nan
            # heart rate
            hr = pt.findall('.//ns1:HeartRateBpm//ns1:Value', ns)
            HeartRate[ct] = float(hr[0].text) if len(hr)>0 else np.nan
            # cadence
            cad = pt.findall('.//ns1:Extensions//ns3:TPX//ns3:RunCadence', ns)
            Cadence[ct] = float(cad[0].text) if len(cad)>0 else np.nan
            # latitude
            lat = pt.findall('.//ns1:Position//ns1:LatitudeDegrees', ns)
            Latitude[ct] = float(lat[0].text) if len(lat)>0 else np.nan
            # longitude
            lon = pt.findall('.//ns1:Position//ns1:LongitudeDegrees', ns)
            Longitude[ct] = float(lon[0].text) if len(lon)>0 else np.nan
            # altitude
            alt = pt.findall('.//ns1:AltitudeMeters', ns)
            Altitude[ct] = float(alt[0].text) if len(alt)>0 else np.nan
            # time (adjust for pauses in data record)
            t = pt.findall('.//ns1:Time', ns)
            tstamp = datetime.datetime.strptime(t[0].text, 
                                                '%Y-%m-%dT%H:%M:%S.%fZ')
            tnet = (tstamp - tstart).total_seconds()
            dt = tnet - oldt
            if dt < 5.0:
                RunTime[ct] = RunTime[ct-1] + dt
            else: # if run was paused for more than 10 sec, only add 1 sec
                RunTime[ct] = RunTime[ct-1] + 1.0
            oldt = tnet
            
            ct += 1

        # format as pandas dataframe
        ptdata = pd.DataFrame({'Distance':Distance, 'RunTime':RunTime, 
                            'Speed':Speed, 'HeartRate':HeartRate, 
                            'Cadence':Cadence, 'Latitude':Latitude, 
                            'Longitude':Longitude, 'Altitude':Altitude})
        # remove duplicate rows due to lap definition in tcx files
        ptdata.drop_duplicates(subset=['RunTime'], inplace=True)
        ptdata.reset_index(drop=True, inplace=True)
        
        return ptdata

    #-------------------------------------------------------------------------
    def GetLapData(self, lapdist=1609.34):
        """
        calculate lap summary data for a desired lap distance (in meters)
        """
        # track point data
        ptdata = self.GetPtData()

        # find indices of closest points to lap markers
        lapidx = [0]
        lap = 1
        while lapidx[-1] < len(ptdata)-2:
            idx = abs(ptdata['Distance'] - lap*lapdist).idxmin()
            if lap == 1: 
                lapidx[0] = idx
            else:
                lapidx.append(idx)
            lap += 1
        
        # indices for computing differences
        id1 = [0] + [i for i in range(1,len(lapidx))]
        id2 = [0] + [i for i in range(len(lapidx)-1)]

        # lap distance
        ld = ptdata['Distance'].iloc[lapidx].reset_index(drop=True)
        Distance = ld.iloc[id1].reset_index(drop=True) - ld.iloc[id2].reset_index(drop=True)
        Distance.iloc[0] = ld.iloc[0]
        
        # lap split
        spl = ptdata['RunTime'].iloc[lapidx].reset_index(drop=True)
        Split = spl.iloc[id1].reset_index(drop=True) - spl.iloc[id2].reset_index(drop=True)
        Split.iloc[0] = spl.iloc[0]
        
        # average speed
        AvgSpeed = Distance/Split

        # elevation gain/loss, heart rate, and cadence
        avghr = np.zeros(len(lapidx))
        avgcad = np.zeros(len(lapidx))
        gain = np.zeros(len(lapidx))
        loss = np.zeros(len(lapidx))
        istart = 0
        for i in range(len(lapidx)):
            # compute average heart rate and cadence over lap
            avghr[i] = ptdata['HeartRate'].iloc[istart:lapidx[i]].mean()
            avgcad[i] = ptdata['Cadence'].iloc[istart:lapidx[i]].mean()
            # sum gains and losses
            da = ptdata['Altitude'].iloc[istart:lapidx[i]].diff()
            gain[i] = da[da>0.0].sum()
            loss[i] = abs(da[da<0.0]).sum()

            istart = lapidx[i]

        # starting and ending positions
        StartLatitude = ptdata['Latitude'].iloc[[2]+lapidx[:-1]].reset_index(drop=True)
        EndLatitude = ptdata['Latitude'].iloc[lapidx].reset_index(drop=True)
        StartLongitude = ptdata['Longitude'].iloc[[2]+lapidx[:-1]].reset_index(drop=True)
        EndLongitude = ptdata['Longitude'].iloc[lapidx].reset_index(drop=True)
        StartAltitude = ptdata['Altitude'].iloc[[0]+lapidx[:-1]].reset_index(drop=True)
        EndAltitude = ptdata['Altitude'].iloc[lapidx].reset_index(drop=True)

        # assemble as dataframe
        lapdata = pd.DataFrame({'Distance':Distance, 'Split':Split, 'AvgSpeed':AvgSpeed,
        'AvgHeartRate':avghr, 'AvgCadence':avgcad,
        'ElevGain':gain, 'ElevLoss':loss,
        'StartLatitude':StartLatitude, 'EndLatitude':EndLatitude,
        'StartLongitude':StartLongitude, 'EndLongitude':EndLongitude,
        'StartAltitude':StartAltitude, 'EndAltitude':EndAltitude})

        return lapdata

    #-------------------------------------------------------------------------
    def GetSummaryData(self):
        """
        calculate activity summary data from lapdata
        """
        # lap summary data
        lapdata = self.GetLapData()

        # start time
        tree = ET.parse(self.filename)
        root = tree.getroot()
        tstart = datetime.datetime.strptime( 
            root.findall('.//ns1:Id', ns)[0].text, '%Y-%m-%dT%H:%M:%S.%fZ' )
        date = tstart.date()
        time = tstart.time()

        # summary data
        TotalDistance = lapdata['Distance'].sum()
        TotalRunTime = lapdata['Split'].sum()
        AvgSpeed = ((lapdata['AvgSpeed']*lapdata['Distance']).sum()/
                    lapdata['Distance'].sum())
        AvgHeartRate = ((lapdata['AvgHeartRate']*lapdata['Distance']).sum()/
                    lapdata['Distance'].sum())
        AvgCadence = ((lapdata['AvgCadence']*lapdata['Distance']).sum()/
                    lapdata['Distance'].sum())
        TotalElevGain = lapdata['ElevGain'].sum()
        TotalElevLoss = lapdata['ElevLoss'].sum()

        # assemble as dataframe
        sumdata = pd.DataFrame({'Date':[date], 'StartTime':[time], 
        'TotalDistance':[TotalDistance], 'TotalRunTime':[TotalRunTime],
        'AvgSpeed':[AvgSpeed], 'AvgHeartRate':[AvgHeartRate],
        'AvgCadence':[AvgCadence], 'TotalElevGain':[TotalElevGain],
        'TotalElevLoss':[TotalElevLoss]})

        sumdata['Date'] = pd.to_datetime(sumdata['Date'])

        return sumdata

    #-------------------------------------------------------------------------
    def PlotLapData(self, lapdist=1.0, units='mi'):
        """
        plot lap summary data for a desired lap distance
        """
        # calculate lap summary data
        if units == 'mi':
            ld = lapdist*1609.34
        elif units == 'km':
            ld = lapdist*1000.00
        lapdata = self.GetLapData(lapdist=ld)
        
        # base time for plotting times on matplotlib axes
        basetime = datetime.datetime(2001,1,1,0,0,0,0)

        # convert speed to pace
        if units == 'mi':
            pace = lapdata['AvgSpeed'].apply(lambda x: np.round(1609.34/x, 
                decimals=0))
            ylab = 'Avg. Pace \n (min/mi)'
            meanpace = self.summary['AvgSpeed'].apply(lambda x: np.round(
                1609.34/x, decimals=0))
        elif units == 'km':
            pace = lapdata['AvgSpeed'].apply(lambda x: np.round(1000.00/x, 
                decimals=0))
            ylab = 'Avg. Pace \n (min/km)'
            meanpace = self.summary['AvgSpeed'].apply(lambda x: np.round(
                1000.00/x, decimals=0))
        pace = pace.apply(lambda x: datetime.timedelta(seconds=x))
        meanpace = meanpace.apply(lambda x: datetime.timedelta(seconds=x))
        # plotting limits
        pmin = basetime + pace.min() - datetime.timedelta(seconds=15)
        pmax = basetime + pace.max() + datetime.timedelta(seconds=15)

        fig, ax = plt.subplots(nrows=4, ncols=1, sharex=True)
        # avg pace
        ax[0].yaxis.set_major_formatter(md.DateFormatter('%M:%S'))
        ax[0].plot(lapdata.index+1, basetime+pace, 'o')
        ax[0].plot([0,len(lapdata)+1], [basetime+meanpace,basetime+meanpace], 
            'r--')
        ax[0].set_ylabel(ylab)
        ax[0].set_ylim((pmin, pmax))
        ax[0].invert_yaxis()
        # net elevation change
        y = lapdata['EndAltitude']-lapdata['StartAltitude']
        if units == 'mi':
            y *= 3.28084
            ylab2 = 'Net Elevation \n Change (ft)'
        elif units == 'km':
            ylab2 = 'Net Elevation \n Change (m)'
        ax[1].plot(lapdata.index+1, y, 'o')
        ax[1].plot([0,len(lapdata)+1], [0.0,0.0], 'r--')
        ax[1].set_ylabel(ylab2)
        # avg cadence
        meancad = self.summary['AvgCadence']*2
        ax[2].plot(lapdata.index+1, lapdata['AvgCadence']*2, 'o')
        ax[2].plot([0,len(lapdata)+1], [meancad,meancad], 'r--')
        ax[2].set_ylabel('Avg. Cadence \n (steps/min)')
        ax[2].set_ylim((145,175))
        # avg heart rate
        meanhr = self.summary['AvgHeartRate']
        ax[3].plot(lapdata.index+1, lapdata['AvgHeartRate'], 'o')
        ax[3].plot([0,len(lapdata)+1], [meanhr,meanhr], 'r--')
        ax[3].set_ylabel('Avg. Heart Rate \n (beats/min)')
        ax[3].set_ylim((120,180))

        plt.xlabel('Lap')
        plt.xlim((0,len(lapdata)+1))
        plt.show()

        return None

    #-------------------------------------------------------------------------
    def PlotPtData(self, units='mi', xvar='Distance'):
        """
        plot instantaneous data versus time or versus distance
        """
        # get point data
        ptdata = self.GetPtData()

        # base time for plotting times on matplotlib axes
        basetime = datetime.datetime(2001,1,1,0,0,0,0)

        # select distance or time as x-variable
        if xvar == 'Distance':
            x = ptdata['Distance'].copy()
            if units == 'mi':
                x = x/1609.34
                xlab = 'Distance (mi)'
            elif units == 'km':
                x = x/1000.00
                xlab = 'Distance (km)'
            xmin = 0.0
            xmax = max(x)
        elif xvar == 'RunTime':
            x = ptdata['RunTime'].apply(lambda x: datetime.timedelta(
                seconds=x)) + basetime
            xlab = 'Run Time (hh:mm:ss)'
            xmin = basetime
            xmax = x.iloc[-1]

        # convert speed to pace
        ptdata.loc[ptdata['Speed']<0.1, 'Speed'] = 0.1
        if units == 'mi':
            pace = ptdata['Speed'].apply(lambda x: np.round(1609.34/x, 
                decimals=0))
            meanpace = self.summary['AvgSpeed'].apply(lambda x: np.round(
                1609.34/x, decimals=0))
            ylab = 'Pace \n (min/mi)'
            pmin = datetime.timedelta(minutes=6,seconds=30)
            pmax = datetime.timedelta(minutes=9,seconds=30)
        elif units == 'km':
            pace = ptdata['Speed'].apply(lambda x: np.round(1000.00/x, 
                decimals=0))
            meanpace = self.summary['AvgSpeed'].apply(lambda x: np.round(
                1000.00/x, decimals=0))
            ylab = 'Pace \n (min/km)'
            pmin = datetime.timedelta(minutes=4,seconds=20)
            pmax = datetime.timedelta(minutes=5,seconds=40)
        pace = pace.apply(lambda x: datetime.timedelta(seconds=x))
        meanpace = meanpace.apply(lambda x: datetime.timedelta(seconds=x))

        fig, ax = plt.subplots(nrows=4, ncols=1, sharex=True)
        # pace
        ax[0].yaxis.set_major_formatter(md.DateFormatter('%M:%S'))
        ax[0].plot(x, basetime + pace)
        ax[0].plot([xmin,xmax], [basetime+meanpace,basetime+meanpace], 'r--')
        ax[0].set_ylabel(ylab)
        ax[0].set_ylim((basetime + pmin, basetime + pmax))
        ax[0].invert_yaxis()
        # elevation
        if units == 'mi':
            y = ptdata['Altitude']*3.28084
            ylab2 = 'Elevation \n (ft)'
        elif units == 'km':
            y = ptdata['Altitude']
            ylab2 = 'Elevation \n (m)'
        ax[1].plot(x, y)
        ax[1].plot([xmin,xmax], [y[0],y[0]], 'r--')
        ax[1].set_ylabel(ylab2)
        # cadence
        meancad = self.summary['AvgCadence']*2
        ax[2].plot(x, ptdata['Cadence']*2, '.')
        ax[2].plot([xmin, xmax], [meancad,meancad], 'r--')
        ax[2].set_ylabel('Cadence \n (steps/min)')
        ax[2].set_ylim((145,175))
        # heart rate
        meanhr = self.summary['AvgHeartRate']
        ax[3].plot(x, ptdata['HeartRate'])
        ax[3].plot([xmin, xmax], [meanhr,meanhr], 'r--')
        ax[3].set_ylabel('Heart Rate \n (beats/min)')
        ax[3].set_ylim((120,180))

        if xvar == 'RunTime':
            ax[3].xaxis.set_major_formatter(md.DateFormatter('%H:%M:%S'))
        plt.xlabel(xlab)
        plt.xlim((xmin, xmax))
        plt.show()

        return None

    #-------------------------------------------------------------------------
    def PlotLapMap(self, lapdist, units, zvar=None, fig=None, ax=None):
        """
        plot lap latitude/longitude data, with optional dependent variable
        """

        if zvar == 'Elevation':
            zvar = 'EndAltitude'

        # calculate lap summary data
        if units == 'mi':
            ld = lapdist*1609.34
        elif units == 'km':
            ld = lapdist*1000.00
        lapdata = self.GetLapData(lapdist=ld)

        x = pd.concat( [ lapdata['StartLongitude'].iloc[0:1],
            lapdata['EndLongitude'] ] ).reset_index(drop=True)
        y = pd.concat( [ lapdata['StartLatitude'].iloc[0:1],
            lapdata['EndLatitude'] ] ).reset_index(drop=True)

        if fig == None or ax == None:
            fig, ax = plt.subplots()
        ax.plot(x, y, '-', color='silver', linewidth=0.5)

        # create segments for multi-color line plot
        points = np.array([x,y]).T.reshape(-1,1,2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)

        if zvar != None:
            if zvar == 'Grade':
                z = (lapdata['EndAltitude']-lapdata['StartAltitude']
                )/lapdata['Distance']*100
                label = 'Percent Grade'
                cmap = 'bwr'
                norm = plt.Normalize(vmin=-12, vmax=12)
            else:
                z = lapdata[zvar].copy()
                if zvar == 'EndAltitude' or zvar == 'StartAltitude':
                    if units == 'mi':
                        z *= 3.28084
                        label = 'Elevation (ft)'
                    else:
                        label = 'Elevation (m)'
                elif zvar == 'AvgSpeed':
                    if units == 'mi':
                        z *= 2.23694
                        label = 'Speed (mph)'
                    else:
                        z*= 3.6
                        label = 'Speed (kph)'

                cmap = 'plasma'
                norm = plt.Normalize(vmin=z.min(), vmax=z.max())
            
            lc = LineCollection(segments, cmap=cmap, norm=norm)
            lc.set_array(z)
            lc.set_linewidth(3.0)
            ax.add_collection(lc)
            fig.colorbar(lc)
        ax.set_aspect('equal', 'box')
        #dx = 0.1*(x.max()-x.min())
        #dy = 0.1*(y.max()-y.min())
        #ax.set_xlim(x.min()-dx, x.max()+dx)
        #ax.set_ylim(y.min()-dy, y.max()+dy)
        ax.xaxis.set_ticks([])
        ax.yaxis.set_ticks([])
        if zvar != None:
            plt.title(label)

        return fig, ax

    #-------------------------------------------------------------------------
    def PlotMap(self, lapdist=1.0, units='mi', zvar=None):
        """
        plot latitude/longitude data, with optional dependent variable
        """

        # plot with fine lap resolution
        if units == 'mi':
            ldf = 0.02
        elif units == 'km':
            ldf = 0.02*1.60934
        fig, ax = self.PlotLapMap(lapdist=ldf, units=units, zvar=zvar)

        # calculate desired lap resolution
        if units == 'mi':
            ldd = lapdist*1609.34
        elif units == 'km':
            ldd = lapdist*1000.00
        lapdata = self.GetLapData(lapdist=ldd)

        # add lap markers
        for i in range(len(lapdata)-1):
            ax.plot([lapdata['EndLongitude'].iloc[i]], 
                [lapdata['EndLatitude'].iloc[i]], 'ko', markersize=12, 
                zorder=9)
            if i < 9:
                ax.annotate('%d' % (i+1), (lapdata['EndLongitude'].iloc[i], 
                    lapdata['EndLatitude'].iloc[i]), 
                    va='center', ha='center', color='white', zorder=10, 
                    fontsize='medium')
            else:
                ax.annotate('%d'%(i+1), (lapdata['EndLongitude'].iloc[i],
                    lapdata['EndLatitude'].iloc[i]), va='center', 
                    ha='center', color='white', zorder=10, fontsize='x-small')

        return None