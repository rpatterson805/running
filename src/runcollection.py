from tcxactivity import tcxactivity
import pandas as pd
import matplotlib.pyplot as plt

class runcollection:

    def __init__(self, filelist):
        """
        read in activity data files
        """
        # parse each file, compute summary data
        self.runs = []
        for file in filelist:
            self.runs.append( tcxactivity(file) )

        # assemble summary data as dataframe
        self.summary = pd.concat([self.runs[i].summary for i in range(
            len(filelist))]).reset_index(drop=True)

        return None

    #-------------------------------------------------------------------------
    def DateRangeSummary(self, startdate, enddate):
        """
        compute summary data for a date range
        """
        dr = self.summary.copy()
        # index dataframe using date range mask
        mask = (dr['Date']>=startdate) & (dr['Date']<=enddate)
        dr = dr.loc[mask]
        
        # compute summary data
        NumActivities = len(dr)
        TotalDistance = dr['TotalDistance'].sum()
        TotalRunTime = dr['TotalRunTime'].sum()
        AvgSpeed = (dr['AvgSpeed']*dr['TotalDistance']).sum()/dr[
            'TotalDistance'].sum()
        AvgHeartRate = (dr['AvgHeartRate']*dr['TotalDistance']).sum()/dr[
            'TotalDistance'].sum()
        AvgCadence = (dr['AvgCadence']*dr['TotalDistance']).sum()/dr[
            'TotalDistance'].sum()
        TotalElevGain = dr['TotalElevGain'].sum()
        TotalElevLoss = dr['TotalElevLoss'].sum()

        # assemble as dataframe
        datesum = pd.DataFrame({'NumActivities':[NumActivities],
        'TotalDistance':[TotalDistance],'TotalRunTime':[TotalRunTime], 
        'AvgSpeed':[AvgSpeed], 'AvgHeartRate':AvgHeartRate, 
        'AvgCadence':[AvgCadence], 'TotalElevGain':[TotalElevGain], 
        'TotalElevLoss':[TotalElevLoss]})

        return dr, datesum

    #-------------------------------------------------------------------------
    def PlotMapAll(self):
        """
        plot all run data on a map
        """
        ct = 0
        for run in self.runs:
            if ct == 0:
                fig, ax = run.PlotLapMap(lapdist=0.02, units='mi')
            else:
                fig, ax = run.PlotLapMap(lapdist=0.02, units='mi', fig=fig, 
                    ax=ax)
            ct += 1

        ax.set_facecolor('xkcd:plum')
        # add a scale to the plot
        xlims = ax.get_xlim()
        ylims = ax.get_ylim()
        xrange = xlims[1] - xlims[0]
        yrange = ylims[1] - ylims[0]
        x0 = xlims[0] + 0.1*xrange
        y0 = ylims[0]
        mile = 1/60*0.868976 # convert degrees to miles
        km = 1/60*0.539957 # convert degrees to km
        xmile = (x0, x0 + mile) 
        xkm = (x0 + mile - km, x0 + mile) 
        ymile = (y0+0.05*yrange, y0+0.05*yrange)
        ykm = (y0+0.1*yrange, y0+0.1*yrange)
        ax.plot(xmile, ymile, color='silver', linewidth=3)
        ax.plot(xkm, ykm, color='silver', linewidth=2)
        ax.text(x0+1.1*mile, y0+0.05*yrange, '1 mi', color='silver', 
            verticalalignment='center_baseline', fontweight='bold')
        ax.text(x0+1.1*mile, y0+0.1*yrange, '1 km', color='silver', 
            verticalalignment='center', fontweight='bold')

        plt.show()

        return None
            