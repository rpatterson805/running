import os
import matplotlib.pyplot as plt
from runcollection import runcollection

files = []
for root, dirs, filenames in os.walk(os.path.join('..','data')):
    for fname in filenames:
        files.append( os.path.join(root, fname) )

a = runcollection(files)

#a.runs[-1].PlotMap(lapdist=1,units='mi',zvar='Grade')
#a.runs[-1].PlotMap(lapdist=1,units='mi')
a.PlotMapAll()
plt.show()